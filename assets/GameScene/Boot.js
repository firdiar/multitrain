
Game.Boot = function(game){
    //this.map = Phaser.Tilemap;

};


Game.Boot.prototype = {

    preload:function(){

      this.add.image(0,0,'Panel');
      var judul =  this.add.image(720/2 , 1280/2 , 'Tittle1');
      judul.anchor.setTo(0.5);

      this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
      this.scale.pageAlignHorizontally = true;
      this.scale.pageAlignVertically = true;


      this.load.image('TalkBox' , 'assets/Image/tutorial/round_box.png');
      this.load.image('Background' , 'assets/Image/bg.png');
      this.load.image('BackgroundSelectlvl' , 'assets/Image/level page.png');
      this.load.image('BackgroundMainMenu' , 'assets/Image/main menu bg.png');
      this.load.image('StartBtn' , 'assets/Image/play button.png');
      this.load.image('PauseBtn' , 'assets/Image/pause button.png')
      this.load.image('Tittle2' , 'assets/Image/level.png');
      this.load.spritesheet('StageNumber' , 'assets/Image/level number.png',107,124);

      this.load.image('ResultPanel' , 'assets/Image/results page.png');
      this.load.image('PausePanel' , 'assets/Image/pause page.png');
      this.load.image('PlayAgain' , 'assets/Image/play again button.png');
      this.load.image('Next' , 'assets/Image/next button.png');
      this.load.image('Home' , 'assets/Image/home button.png');
      this.load.image('Quit' , 'assets/Image/quit button.png');
      this.load.image('Resume' , 'assets/Image/resume button.png');
      this.load.image('Restart' , 'assets/Image/restart button.png');
      this.load.image('ScorePanel' , 'assets/Image/time and correct.png');
      this.load.image('Gerbong' , 'assets/Image/train/gerbong.png');
      this.load.image('Train' , 'assets/Image/train/kereta.png');
      this.load.image('Rail' , 'assets/Image/rail/rel lurus.png');
      this.load.image('RailWood' , 'assets/Image/rail/relwood.png');
      this.load.image('TrainSpawner' , 'assets/Image/building/spawner.png');
      this.load.image('TrainStation' , 'assets/Image/building/station.png');
      this.load.image('RailBelok' , 'assets/Image/rail/reChanger.png');
      this.load.image('MultiRail' , 'assets/Image/reChanger.png');

      this.load.image('Pohon' , 'assets/Image/decoration/pohon.png');
      this.load.image('Batu' , 'assets/Image/decoration/batu.png');
      this.load.image('Kayu' , 'assets/Image/decoration/kayu.png');

      this.load.spritesheet('MultiRail2' , 'assets/Image/railchanger.png',102,100);

      //level json
      this.load.text('Graph1' , 'assets/json/graph/lvl1map.json');
      this.load.text('Graph2' , 'assets/json/graph/lvl2map.json');
      this.load.text('Graph3' , 'assets/json/graph/lvl3map.json');
      this.load.text('Graph4' , 'assets/json/graph/lvl4map.json');
      this.load.text('Graph5' , 'assets/json/graph/lvl5map.json');
      this.load.text('Graph6' , 'assets/json/graph/lvl6map.json');
      this.load.text('Graph7' , 'assets/json/graph/lvl7map.json');
      this.load.text('Graph8' , 'assets/json/graph/lvl8map.json');
      this.load.text('Graph9' , 'assets/json/graph/lvl9map.json');
      this.load.text('Graph10' , 'assets/json/graph/lvl10map.json');



    },
    create:function(){

        this.state.start('MainMenu');

    }


}


function station (obj , kode){
  this.pos = obj,
  this.kode = kode
  this.kill = function(){
      this.pos.kill();
      this.kode = '';

  }
}

function rail (obj , branch) {
  this.pos = obj,
  this.branch = branch,
  this.getNext = function(){
      return this.branch;
  }
  this.kill = function(){
      this.pos.kill();
      this.branch = null;

  }
}


function railBranch (obj , key , branch , FAR) {
  this.next = null;
  this.key = key,
  this.pos = obj,
  this.branch = branch,
  this.FAR = FAR,

  this.getNext = function(){
    if(this.branch != null){
      return this.branch[this.key];
    }
  }
  this.changeBranch = function(){
      this.key++;
      console.log('change route : ',this.key);
      if(this.key >= this.branch.length){
        this.key = 0;
      }
      this.pos.angle = FAR[this.key].angle;
      this.pos.frame = FAR[this.key].frame;
      if(this.pos.frame == 1){
          this.pos.anchor.setTo(0.522,0.5);
      }else{
          this.pos.anchor.setTo(0.45,0.39);
      }

  }
  this.kill = function(){
      this.pos.kill();
      this.key = null;
      this.branch = null;
      this.next = null;

  }
}

function Node(ID){
    //this.next = null;
    this.ID = ID,
    this.stasiun = false,
    this.rail = null,
    this.kill = function(){
        this.rail.kill();
        this.rail = null;
        this.ID = null;
    }
}


(function(){Math.clamp=function(a,b,c){return Math.max(b,Math.min(c,a));}})();
