Game.Lvl = function(game){


}

// ini akan diisi dengan json file
var data;
var myHS = {
    score : 0,
    fail:0,
    total:0
}


Game.Lvl.prototype = {

    create:function(){


      ///loading data level and insert to variable
      data = JSON.parse(this.cache.getText(PATH.PATH));


      data.finish = false;

      myHS.score = 0;

      myHS.fail = 0;
      myHS.total = 0;




      //adding background
      data.bg = this.add.image(0 , 0 ,'Background');


      data.exitPanel = this.add.image(500,700,'Panel');//layer
      data.exitPanel.anchor.setTo(0.7 , 0.545);
      data.exitPanel.scale.setTo(1.5);
      data.exitPanel.alpha = 0;

      data.board = this.add.image(720/2 , 1280/2 , 'ResultPanel');//panel bg aja
      data.board.anchor.setTo(0.5);
      data.board.scale.setTo(1.1);
      data.board.alpha = 0;

      data.board2 = this.add.image(720/2 , 1280/2 , 'PausePanel');//panel bg aja
      data.board2.anchor.setTo(0.5);
      data.board2.scale.setTo(1.1);
      data.board2.alpha = 0;

      data.exitbtn = this.add.image(360 , 850 , 'Quit');
      data.exitbtn.anchor.setTo(0.5);
      data.exitbtn.inputEnabled = false;
      data.exitbtn.alpha = 0;
      data.exitbtn.scale.setTo(1.2);
      data.exitbtn.events.onInputDown.add(()=>this.changeScene('MainMenu') , this);



      data.reStartbtn = this.add.image(360 , 700 , 'Restart');
      data.reStartbtn.anchor.setTo(0.5);
      data.reStartbtn.inputEnabled = false;
      data.reStartbtn.alpha = 0;
      data.reStartbtn.scale.setTo(1.2);
      data.reStartbtn.events.onInputDown.add(()=>this.changeScene('LevelGenerators') , this);

      data.pausebtn2 = this.add.image(360 , 550 , 'Resume');
      data.pausebtn2.anchor.setTo(0.5);
      data.pausebtn2.inputEnabled = false;
      data.pausebtn2.alpha = 0;
      data.pausebtn2.scale.setTo(1.2);
      data.pausebtn2.events.onInputDown.add(this.pauseGame , this);




      data.railObj = this.add.group();



      data.scorepanel = this.add.image(510 , 65 , 'ScorePanel');
      data.scorepanel.text = this.add.text(  data.scorepanel.x+100 ,   data.scorepanel.y , myHS.score+" of "+0 ,{
        font: "32px Arial",
        fill: "#000000",
        align: "center"
      });

      data.scorepanel.text2 = this.add.text(  data.scorepanel.x-60 ,   data.scorepanel.y , '00:00' ,{
        font: "32px Arial",
        fill: "#000000",
        align: "center"
      });

      data.scorepanel.anchor.setTo(0.5);
      data.scorepanel.scale.setTo(0.75);
      data.scorepanel.text.anchor.setTo(0.5);
      data.scorepanel.text2.anchor.setTo(0.5);






      //adding kode , kode is data brought by train and own by staation , if same then poin +1 else health -1
      data.kode = data.kode;

      //setting map
      this.initMap();





      //setting spawner
      this.initSpawner();

      data.graphics = this.add.graphics(0,0);

      //setting train , membuat train group
      data.gerbong = this.add.group();
      data.train = this.add.group();

      //mengatur agar semua bangunan ada di posisi teratas
      var tempPointer = data.spawnManager.map.first;
      while(tempPointer!= null){

        if(tempPointer.rail.kode != null){
          this.world.bringToTop(tempPointer.rail.pos);
        }
        tempPointer = tempPointer.next;

      }

      this.world.bringToTop(data.spawnManager.spawner);

      data.PauseBtn = this.add.image(100 , 65 , 'PauseBtn');
      data.PauseBtn.anchor.setTo(0.5);
      data.PauseBtn.inputEnabled = true;
      data.PauseBtn.events.onInputDown.add(this.pauseGame , this);
      data.isPaused = false;


      //spawning decoration
      data.mapStatic.forEach(function(obj){
          switch(obj.type){
            case 0:
                this.add.image(obj.x , obj.y , 'Pohon');
              break;

            case 1:
                this.add.image(obj.x , obj.y , 'Kayu');
              break;

            case 2:
                  this.add.image(obj.x , obj.y , 'Batu');
                break;
          }

      }.bind(this));



      if(PATH.PATH == 'Graph1'){

        this.spawnTrain();
        this.time.events.add(Phaser.Timer.SECOND * 3, this.startTutorial, this)
      }




    },
    startTutorial:function(){
      this.pauseGame();
      data.board2.alpha = 0;

      data.exitbtn.inputEnabled = false;
      data.exitbtn.alpha = 0;

      data.reStartbtn.inputEnabled = false;
      data.reStartbtn.alpha = 0;

      data.pausebtn2.inputEnabled = false;
      data.pausebtn2.alpha = 0;

      this.talkBox = this.add.image(525 , 1000 , 'TalkBox');
      this.talkBox.anchor.setTo(0.5);
      this.talkBox.scale.setTo(0.7 , 0.5);
      this.talkBox.inputEnabled = true;


      this.talkBox.text = this.add.text(  this.talkBox.x , this.talkBox.y, 'Wahh lihat keretanya jalan\nArahkan Kereta itu ke stasiun\nyang warnanya sama\n\nClick To Next',{
        font: "22px Arial",
        fill: "#000000",
        align: "center",
        stroke: '#000000',
        strokeThickness: 1,
        fontWeight:"bold"
      });
      this.talkBox.text.anchor.setTo(0.5);
      this.talkBox.events.onInputDown.add(this.tutorialTalk, this);

    },
    tutorialTalk:function(){
      if(this.talkBox.text.text == 'Wahh lihat keretanya jalan\nArahkan Kereta itu ke stasiun\nyang warnanya sama\n\nClick To Next' ){
        this.talkBox.text.setText('Kamu bisa mengubah jalan\ndengan menekannya jalan\nyang berwarna biru\n\nClick To Next');
      }else if(this.talkBox.text.text == 'Kamu bisa mengubah jalan\ndengan menekannya jalan\nyang berwarna biru\n\nClick To Next'){
        this.talkBox.text.kill();
        this.talkBox.kill();
        this.pauseGame();
      }

    },
    update:function(){
        if(data.isPaused){
            return;
        }

        this.moveTrain();



    },
    moveTrain:function(){
          data.graphics.clear();
          data.graphics.lineStyle(8, 0x000000, 1);
          //menggerakan semua kereta
          data.train.forEach(function(c){
              var pos = {
                x : 0,
                y : 0
              }
              var posD = {
                x : 0,
                y : 0
              }
              var delta = {
                x:0,
                y:0
              }



              //mencari jarak kereta dengan tujuan
              delta.x = c.nextPath.rail.pos.x - c.x;
              delta.y= c.nextPath.rail.pos.y - c.y;


              var i = 1.5;
              if((delta.x != 0) && (delta.y != 0))
                  i = 1.3;


              //menyimpan posisi lama
              pos.x = c.x;
              pos.y = c.y;

              //memperbarui posisi/bergerak lokomotif
              if(delta.x > 0){
                  c.x = Math.clamp(c.x + i, c.x, c.nextPath.rail.pos.x);

              }else if(delta.x < 0){
                  c.x = Math.clamp(c.x - i, c.nextPath.rail.pos.x, c.x);

              }

              if(delta.y > 0){
                  c.y = Math.clamp(c.y + i, c.y, c.nextPath.rail.pos.y);
              }else if(delta.y < 0){
                  c.y = Math.clamp(c.y - i, c.nextPath.rail.pos.y, c.y);
              }

              //mencari delta perubahan posisi kereta
              posD.x =c.x - pos.x;
              posD.y =c.y - pos.y;



              //mencari derajat antara vector saat ini dengan vector (0,1)
              var degree = this.getDegree(posD.x,posD.y,c.angle);
              //console.log(degree);


              //mencari perbedaan derajat lama dan derajat baru(yang dituju)
              var deltaDegree = degree - c.angle;

              //mengetahui apa putaran keatas atau kebawah
              var pemicu = true;
              if(Math.abs(deltaDegree) > 170){

                deltaDegree = degree - (360 + c.angle);



                pemicu = false;
                //console.log(posD.y,'  ',posD.x);

              }

              //memutar derajat kereta
              if( deltaDegree > 0){

                  if(pemicu){c.angle = Math.clamp(c.angle + (130*this.time.elapsed/1000),   c.angle , degree);}
                  else {c.angle += 2;}

              }else if(deltaDegree<0){

                  if(pemicu){c.angle = Math.clamp(c.angle - (130*this.time.elapsed/1000), degree , c.angle);}
                  else{
                    c.angle -= 2;}

              }

              //menyimpan data posisi kereta dan derajat kereta saat ini
              c.queue.add({x:c.x , y : c.y});
              c.queue2.add({angle:c.angle});




              //menggerakan rotasi gerbong kereta
              if(c.queue2.total > 28){
                  c.gerbong.angle = c.queue2.first.angle;
                  c.queue2.remove(c.queue2.first);

              }

              //menggerakan posisi gerbong kereta
              if(c.queue.total > 34){
                  c.gerbong.x = c.queue.first.x;
                  c.gerbong.y = c.queue.first.y;
                  c.queue.remove(c.queue.first);


                }


                data.graphics.moveTo(c.x , c.y);
                data.graphics.lineTo(c.gerbong.x , c.gerbong.y);





              if((Math.abs(delta.x)+Math.abs(delta.y)) <30){//mengecek sisa jarak dengan tujuan

                    if(c.nextPath.rail.kode == null){//mengecek apakah tujuan adalah stasiun / jika tujuan bukan stasiun
                        c.nextPath = c.nextPath.rail.getNext();


                    }else if((Math.abs(delta.x)+Math.abs(delta.y)) == 0){  //jika tujuan stasiun
                        if(c.kode == c.nextPath.rail.kode){//jika kereta memasuki stasiun yang benar
                            console.log('Stasiun Benar : point + 1');
                            //do something
                            myHS.score += 1;
                            myHS.total = myHS.score+myHS.fail;


                        }else{//jika keteta memasuki stasiun yang salah
                            console.log('Stasiun salah : hp - 1');
                            //do something
                            myHS.fail++;

                            myHS.total = myHS.score+myHS.fail;


                        }
                        data.scorepanel.text.setText(myHS.score+" of "+myHS.total);
                        //menghapus kereta ini
                        c.queue.reset();
                        c.queue2.reset();
                        data.gerbong.remove(c.gerbong);
                        c.gerbong.kill();
                        c.kill();
                        c.nextPath = null;
                        c.kode = '';
                        data.train.remove(c);
                    }
            }




          }.bind(this));

          if(((data.spawnManager.maxTime <= 0 && data.train.length == 0) ) &&!data.finish){
                  //game selesai

                  this.world.bringToTop(data.exitPanel);//panel layer
                  this.world.bringToTop(data.board);

                  data.board.alpha = 1;





                  //this.world.bringToTop(data.PauseBtn);

                  this.add.text(  135 , 580, 'correct :   '+myHS.score+" of "+myHS.total,{
                    font: "50px Arial",
                    fill: "#4E342E",
                    align: "center",
                    stroke: '#000000',
                    strokeThickness: 8,
                    fontWeight:"bold"
                  });

                  this.add.text(  135 , 680, 'Level    :   '+PATH.currentLevel,{
                    font: "50px Arial",
                    fill: "#4E342E",
                    align: "center",
                    stroke: '#000000',
                    strokeThickness: 8,
                    fontWeight:"bold"
                  });

                  data.PauseBtn.inputEnabled = false;
                  data.exitPanel.alpha = 0.5;

                  //exit go home
                  data.exitbtn2 = this.add.image(180 , 850 , 'Home');
                  data.exitbtn2.anchor.setTo(0.5);
                  data.exitbtn2.inputEnabled = true;
                  data.exitbtn2.events.onInputDown.add(()=>this.changeScene('MainMenu') , this);
                  data.exitbtn2.alpha = 1;

                  if(data.spawnManager.maxTime <= 0 && data.train.length == 0  && !data.finish ){//menang
                      data.finish = true;

                      //next level
                      if(data.nextLevel != ''){
                            data.nextbtn = this.add.image(415 , 860, 'Next');

                            data.nextbtn.anchor.setTo(0.5);
                            data.nextbtn.inputEnabled = true;
                            data.nextbtn.events.onInputDown.add(()=>this.changeScene('LevelGenerator') , this);
                      }

                  }






        }

    },
    changeScene:function(name){
          //menghapus map yang ada
          pointer = data.spawnManager.map.first;
          while (pointer != null) {
              pointer.kill();
              pointer = pointer.next;
          }
          data.railObj.forEach(function(obj){
              obj.kill();
          });
          data.board.kill();
          data.scorepanel.text.kill();
          data.scorepanel.kill();
          data.bg.kill();
          data.exitbtn.kill();
          data.spawnManager.spawner.kill();
          if(name == 'LevelGenerator'){
              if(data.nextLevel != ''){
                  PATH.PATH = data.nextLevel;
                  PATH.currentLevel++;
              }else{
                  name = 'MainMenu';
              }


          }else if (name == 'LevelGenerators') {
              name = 'LevelGenerator'
          }

          target = name
          name = 'Loading';


          this.state.start(name);
    },
    pauseGame:function(){
      data.isPaused = !data.isPaused;
      data.exitPanel.alpha = 0.5;

      this.world.bringToTop(data.exitPanel);
      this.world.bringToTop(data.board2);
      this.world.bringToTop(data.reStartbtn);
      this.world.bringToTop(data.exitbtn);
      this.world.bringToTop(data.PauseBtn);
      this.world.bringToTop(data.pausebtn2);

      if(data.isPaused){
          data.spawnManager.timer.pause();
          data.spawnManager.timer2.pause();
          data.reStartbtn.inputEnabled = true;
          data.pausebtn2.inputEnabled = true;

          data.pausebtn2.alpha = 1;
          data.reStartbtn.alpha = 1;

          data.board2.alpha = 1;

          data.exitbtn.inputEnabled = true;
          data.exitbtn.alpha = 1;


      }else{
          data.spawnManager.timer.resume();
          data.spawnManager.timer2.resume();
          data.exitPanel.alpha = 0;
          data.board2.alpha = 0;

          data.exitbtn.inputEnabled = false;
          data.exitbtn.alpha = 0;

          data.reStartbtn.inputEnabled = false;
          data.reStartbtn.alpha = 0;

          data.pausebtn2.inputEnabled = false;
          data.pausebtn2.alpha = 0;

      }

      var pointer = data.spawnManager.map.first;
      while (pointer!=null) {
        pointer.rail.pos.inputEnabled = !data.isPaused;
        pointer = pointer.next;

      }


    },
    spawnTrain:function(){

        //mengecek kereta yang dikeluarkan sudah sesuai dan mengundi apakah dia bisa di spawn sesuai success rate dan fail rate
        if(Math.floor((Math.random() * (data.spawnManager.succesSpawnRate+data.spawnManager.failSpawnRate))) < data.spawnManager.succesSpawnRate) {


          var tempGerbong = data.gerbong.add(this.add.sprite(data.spawnManager.spawner.x , data.spawnManager.spawner.y ,'Gerbong'));
          var tempTrain = data.train.add(this.add.sprite(data.spawnManager.spawner.x , data.spawnManager.spawner.y ,'Train'));

          tempTrain.gerbong = tempGerbong;

          tempTrain.queue = new Phaser.LinkedList();
          tempTrain.queue2 = new Phaser.LinkedList();
          tempTrain.queue.done = false;
          tempTrain.queue2.done = false;
          tempTrain.angle = data.spawnManager.trainStartAngle;
          tempGerbong.angle = data.spawnManager.trainStartAngle;
          tempGerbong.anchor.setTo(0.5);
          tempGerbong.scale.setTo(-0.8 , 0.8);

          //menyesuaikan tampilan kereta
          tempTrain.anchor.setTo(0.5);
          tempTrain.scale.setTo(-0.75 , 0.75);

          //memasukan data tujuan awal kereta
          tempTrain.nextPath = data.spawnManager.map.first;

          //memasukan kode dan warna , player harus menunjukan kereta ke stasiun yang memiliki kode dan warna sama
          var random = Math.floor((Math.random() * data.kode.length));
          tempTrain.kode = data.kode[random].kode;
          tempTrain.tint = data.kode[random].tint;
          if(data.kode[random].secondTint == null){
              tempGerbong.tint = tempTrain.tint;
          }else{
              tempGerbong.tint = data.kode[random].secondTint;
          }
        }

    },
    timeUpdate:function(){
        data.spawnManager.maxTime--;
        var time ={
          m:0,
          s:0
        }

        time.m = Math.floor(data.spawnManager.maxTime/60);
        time.s = data.spawnManager.maxTime -  time.m*60;
        var m ='';
        var s = '';
        if(time.m < 10){
           m = '0';
        }
        if(time.s < 10){
          s = '0'
        }
        data.scorepanel.text2.setText(m+time.m+':'+s+time.s);
        //console.log(Math.floor(data.spawnManager.maxTime/60),':',data.spawnManager.maxTime -  (Math.floor(data.spawnManager.maxTime/60)*60));
        if(data.spawnManager.maxTime<=0){
           data.spawnManager.timer.stop();
           data.spawnManager.timer2.stop();
        }

    },
    initSpawner:function(){
        data.spawnManager.spawner = this.add.image(data.spawnManager.pos.x,data.spawnManager.pos.y,'TrainSpawner');
        data.spawnManager.spawner.anchor.setTo(0.5,0.44);
        data.spawnManager.spawner.scale.setTo(1.15);
        data.spawnManager.timer = this.time.create(false);
        data.spawnManager.timer.loop(data.spawnManager.repeatTime , this.spawnTrain , this);
        data.spawnManager.timer.start();

        data.spawnManager.timer2 = this.time.create(false);
        data.spawnManager.timer2.loop(1000 , this.timeUpdate , this);
        data.spawnManager.timer2.start();

        data.spawnManager.spawner.angle = this.getDegree((data.spawnManager.map.first.rail.pos.x - data.spawnManager.pos.x) ,
                                                          (data.spawnManager.map.first.rail.pos.y - data.spawnManager.pos.y));
        var delta = {x:0,y:0}
        delta.x =  data.spawnManager.map.first.rail.pos.x - data.spawnManager.spawner.x;
        delta.y =  data.spawnManager.map.first.rail.pos.y - data.spawnManager.spawner.y;

        this.createRoad(delta.x,delta.y,data.spawnManager.spawner.x , data.spawnManager.spawner.y,50,'RailWood',-1);
        this.createRoad(delta.x,delta.y,data.spawnManager.spawner.x , data.spawnManager.spawner.y,50,'Rail',-1);

    },
    initMap:function(){//menyiapkan map
        data.spawnManager.map = new Phaser.LinkedList();

        //memasukan semua node yang ada(belum termasuk info)
        for (var key in  data.graph){
              node = data.graph[key];
              data.spawnManager.map.add(new Node(node.ID));
              if(node.type == 3){
                  data.spawnManager.map.last.stasiun = true;
              }
          }

          //memasukan info kedalam node dan menghubungkan node 1 dan node lain
          pointer = data.spawnManager.map.first;
          for (var key in data.graph){

            node = data.graph[key];

            if(node.type == 1){//rel belok biasa
                var branch;
                //mencari node yang dituju setelah node ini
                var tempPointer = data.spawnManager.map.first;
                while((tempPointer!= null) && (tempPointer.ID != node.connection)){
                  tempPointer = tempPointer.next;
                }
                if(tempPointer != null){
                    branch = tempPointer;
                    if(branch.stasiun){

                        branch.prevs = pointer;
                    }

                }






                //memasukan data
                pointer.rail = new rail( this.add.sprite(node.x , node.y , 'RailBelok'), branch);
                pointer.rail.pos.angle = node.angle;
                pointer.rail.pos.anchor.setTo(0.785 , 0.82);






            }else if(node.type == 2){//rel belok bercabang
                var branch
                branch = [];
                var i = 0;


                for(var j = 0 ; j < node.connection.length ; j++){

                      var tempPointer = data.spawnManager.map.first;
                      while((tempPointer!= null) && (tempPointer.ID != node.connection[i])){
                        tempPointer = tempPointer.next;
                      }
                      if(tempPointer != null){
                        branch[i] = tempPointer;

                        if(branch[i].stasiun){

                            branch[i].prevs = pointer;
                        }

                        i++;
                      }

                }

                //memasukan data
                pointer.rail = new railBranch( this.add.sprite(node.x , node.y, 'MultiRail2') /*this.add.sprite(node.x , node.y , 'MultiRail')*/ , 0 , branch , node.FAR);
                pointer.rail.pos.frame = pointer.rail.FAR[pointer.rail.key].frame;
                pointer.rail.pos.angle = pointer.rail.FAR[pointer.rail.key].angle;
                if(pointer.rail.pos.frame == 1){
                    pointer.rail.pos.anchor.setTo(0.522,0.5);
                }else{
                    pointer.rail.pos.anchor.setTo(0.45,0.39);
                }

                pointer.rail.pos.scale.setTo(1)

                //membuat cabang dapat di klik
                pointer.rail.pos.inputEnabled = true;

                pointer.rail.pos.events.onInputDown.add(pointer.rail.changeBranch , pointer.rail);

            }else if(node.type == 3){//stasiun
                //memasukan data
                pointer.rail = new station(this.add.sprite(node.x , node.y , 'TrainStation') , data.kode[node.kode].kode);
                pointer.rail.pos.anchor.setTo(0.5);

                //memberikan warna ke stasiun
                pointer.rail.pos.tint = data.kode[node.kode].tint;

                //this.game.world.addAt(pointer.rail.pos , 3);
                pointer.rail.pos.angle = this.getDegree((pointer.prevs.rail.pos.x - node.x) , (pointer.prevs.rail.pos.y - node.y));


            }
            pointer = pointer.next;
          }

          var delta = {
            x : 0,
            y:0
          }

          pointer = data.spawnManager.map.first;

          while (pointer != null ) {
              if(pointer.rail.branch != null && !Array.isArray(pointer.rail.branch)){
                  delta.x =  pointer.rail.branch.rail.pos.x - pointer.rail.pos.x;
                  delta.y =  pointer.rail.branch.rail.pos.y - pointer.rail.pos.y;

                  this.createRoad(delta.x,delta.y,pointer.rail.pos.x,pointer.rail.pos.y,50,'RailWood',-1);
                  if(Math.abs(delta.x+delta.y) -120 > 0){
                    this.createRoad(delta.x,delta.y,pointer.rail.pos.x,pointer.rail.pos.y,50,'Rail',-1);
                  }else{
                    this.world.bringToTop(pointer.rail.branch.rail.pos);
                  }
              }else if(pointer.rail.branch != null && Array.isArray(pointer.rail.branch)){
                  for(var j = 0 ; j<pointer.rail.branch.length ; j++){

                      delta.x =  pointer.rail.branch[j].rail.pos.x - pointer.rail.pos.x;
                      delta.y =  pointer.rail.branch[j].rail.pos.y - pointer.rail.pos.y;
                      this.createRoad(delta.x,delta.y,pointer.rail.pos.x,pointer.rail.pos.y,50,'RailWood',-1);
                      if(Math.abs(delta.x+delta.y) -120 > 0){
                        this.createRoad(delta.x,delta.y,pointer.rail.pos.x,pointer.rail.pos.y,50,'Rail',-1);
                      }


                  }

              }
              pointer = pointer.next;

          }







    },
    getDegree:function(x , y , anglenow ){
        var a = Math.sqrt((x*x) +(y*y)) ;
        var i = -1;
        y = y/a;
        if(y > 0){
            i = 1;
        }else if(y==0 && anglenow > 0){
           i = 1
        }
        return i * (Math.acos(x/a)*(180/Math.PI));
    },
    createRoad:function(deltax , deltay , originx , originy , dist , name,offset){
      var tempwood;


      if(deltax == 0){
        if(name != 'Rail'){
          for(var i=1 ; i <= Math.floor(Math.abs(deltay) / dist)+offset ; i++){
            tempwood = this.add.image( originx ,  originy + i*dist*(Math.abs(deltay)/deltay),name);
            tempwood.anchor.setTo(0.5);
            data.railObj.add(tempwood);
          }
        }else if(Math.abs(deltay)-120 > 0){
            tempwood = this.add.image( originx ,  originy+(deltay/2),name);
            tempwood.anchor.setTo(0.5);
            tempwood.height = Math.abs(deltay)-100;
        }
      }else if(deltay == 0){

        if(name != 'Rail'){
          for(var i=1 ; i <= Math.floor(Math.abs(deltax) / dist )+offset ; i++){
            tempwood = this.add.image( originx + i*dist*(Math.abs(deltax)/deltax),  originy ,name);

            tempwood.anchor.setTo(0.5);
            tempwood.angle = 90;
            data.railObj.add(tempwood);
          }
        }else if(Math.abs(deltax)-120 > 0){


          tempwood = this.add.image( originx +(deltax/2) ,  originy,name);
          tempwood.anchor.setTo(0.5);
          tempwood.height = Math.abs(deltax)-100;
          tempwood.angle = 90;
        }
      }


    }



}
